<%-- 
    Document   : liste
    Created on : 17 nov. 2022, 17:29:11
    Author     : Valiah Karen
--%>
<%@page import="Model.ModelView"%>
<% ModelView mv = (ModelView)request.getAttribute("mv");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <h3><% out.print(mv.getData().get("Bienvenue"));%></h3>
        <h3><% out.print(mv.getData().get("Bonjour"));%></h3>
    </body>
</html>
