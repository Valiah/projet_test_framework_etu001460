<%-- 
    Document   : olona
    Created on : 18 nov. 2022, 16:00:07
    Author     : Valiah Karen
--%>

<%@page import="Model.ModelView"%>
<% ModelView mv = (ModelView)request.getAttribute("mv");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <h3><% out.print(mv.getData().get("nom"));%></h3>
        <h3><% out.print(mv.getData().get("prenom"));%></h3>
        <h3><% out.print(mv.getData().get("id"));%></h3>
    </body>
</html>
