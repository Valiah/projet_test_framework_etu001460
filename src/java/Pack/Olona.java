/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pack;
import Annotation.AnnotationMethod;
import Model.ModelView;
import java.util.HashMap;
/**
 *
 * @author Valiah Karen
 */
public class Olona {
     private int id;
    private String nom;
    private String prenom;

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @AnnotationMethod(name = "lire")
    public String lire() {
        return "read";
    }

    @AnnotationMethod(name = "lister")
    public ModelView lister() {
        ModelView m = new ModelView();
        HashMap<String, Object> data = new HashMap<>();
        String lire = this.lire();
        //data.put("read", lire);
        data.put("Bienvenue", "Bienvenue ! ");
        data.put("Bonjour", "Bonjour");
        m.setData(data);
        m.setUrlJsp("liste.jsp");
        return m;
    }

    @AnnotationMethod(name = "olona")
    public ModelView olona() {
        ModelView m = new ModelView();
        HashMap<String, Object> data = new HashMap<>();
        String lire = this.lire();
        System.out.println("lire = "+lire);
        //data.put("read", lire);
        data.put("nom", this.getNom());
        data.put("prenom", this.getPrenom());
        data.put("id", this.getId());
        m.setData(data);
        m.setUrlJsp("olona.jsp");
        return m;
    }   
    
    
    
}
